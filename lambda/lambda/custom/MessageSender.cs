﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using VoiceToFactoryMessages.Messages;

namespace lambda
{
    public class MessageSender
    {
        const string IotEndpoint = "a2dqsjwbyvfvwy-ats.iot.us-east-2.amazonaws.com";
        private const int BrokerPort = 8883;
        public MqttClient IotClient;

        public void ConnectWithMqttBroker()
        {
            string path = Directory.GetCurrentDirectory();
            var caCert = X509Certificate.CreateFromCertFile($"{path}/Certificates/root-CA.crt");
            var clientCert = new X509Certificate2($"{path}/Certificates/dotnet_devicecertificate.pfx", "uranio32");
            string clientId = Guid.NewGuid().ToString();
            IotClient = new MqttClient(IotEndpoint, BrokerPort, true, caCert, clientCert,
                MqttSslProtocols.TLSv1_2);
            IotClient.Connect(clientId);
        }

        public bool SendLineStatusMessage(string line, bool status)
        {
            try
            {
                string topic = $"Line{line}/machineOrder";
                LineStatusOrder order = new LineStatusOrder(line, status);
                var message = JsonConvert.SerializeObject(order);
                IotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public bool SendLineVelocityMessage(string line, int velocity, string action)
        {
            try
            {
                string topic = $"Line{line}/machineOrder";
                LineChangeVelocityOrder order = new LineChangeVelocityOrder(line,action ,velocity);
                var message = JsonConvert.SerializeObject(order);
                IotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        internal bool SendAddUnitToWorkOrder(int order, int unit, string action)
        {
            try
            {
                string topic = $"workorder/workorder";
                AddUnit addUnit = new AddUnit(order.ToString(), unit);
                var message = JsonConvert.SerializeObject(addUnit);
                IotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal bool SendAddMeasurementToWorkOrder(int order, string measurementType)
        {
            try
            {
                string topic = $"workorder/workorder";
                AddMeasurement addMeasurement = new AddMeasurement(order.ToString(), measurementType, string.Empty);
                var message = JsonConvert.SerializeObject(addMeasurement);
                IotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal bool SendAddActionOverWorkOrder(string action, string workorder, string line)
        {
            try
            {
                string topic = $"Line{line}/machineOrder";
                WorkOrderAction workOrderAction = new WorkOrderAction(action, workorder, line);
                var message = JsonConvert.SerializeObject(workOrderAction);
                IotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}