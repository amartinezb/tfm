﻿using Newtonsoft.Json;

namespace lambda.AlexaAPI.Request
{
    public class Application
    {
        [JsonProperty("applicationID")]
        public string ApplicationId {get; set;}
    }
}
