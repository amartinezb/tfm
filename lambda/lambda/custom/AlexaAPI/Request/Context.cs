﻿using Newtonsoft.Json;

namespace lambda.AlexaAPI.Request
{
    public class Context
    {
        [JsonProperty("System")]
        public SystemObject System {get; set;}

//   [JsonProperty("AudioPlayer")]
//   public AudioPlayer AudioPlayer {get; set;}
    }
}
