﻿using Newtonsoft.Json;

namespace lambda.AlexaAPI.Request
{
    public class Slot
    {
        [JsonProperty("name")]
        public string Name {get; set;}

        [JsonProperty("value")]
        public string Value {get; set;}
    }
}
