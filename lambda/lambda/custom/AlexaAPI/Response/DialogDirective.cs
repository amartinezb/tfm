﻿using Newtonsoft.Json;

namespace lambda.AlexaAPI.Response
{
    public class DialogDirective : IDirective
    {
        [JsonProperty("type")]
        public string Type {get; set;}
    }
}
