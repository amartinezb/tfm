﻿namespace lambda.AlexaAPI.Response
{
    public interface IOutputSpeech 
    {
        string Type {get;}
    }
}
