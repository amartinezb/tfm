﻿using Newtonsoft.Json;

namespace lambda.AlexaAPI.Response
{
    public class Reprompt
    {
        [JsonProperty("outputSpeech", NullValueHandling = NullValueHandling.Ignore)]
        public IOutputSpeech OutputSpeech {get; set;}
    }
}
