
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Amazon.Lambda.Core;
using lambda.AlexaAPI;
using lambda.AlexaAPI.Request;
using lambda.AlexaAPI.Response;
using Newtonsoft.Json;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace lambda
{
    /// <summary>
    /// Function Class
    /// </summary>
    public class Function
    {
        /// <summary>
        /// The response
        /// </summary>
        private SkillResponse _response;
        /// <summary>
        /// The line data
        /// </summary>
        private static LineData _lineData;
        /// <summary>
        /// The context
        /// </summary>
        private ILambdaContext _context;
        /// <summary>
        /// The localename
        /// </summary>
        const string Localename = "locale";
        /// <summary>
        /// The spain locale
        /// </summary>
        const string SpainLocale = "es-ES";
        /// <summary>
        /// The sender
        /// </summary>
        private MessageSender _sender;

        /// <summary>
        /// Application entry point
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="ctx">The CTX.</param>
        /// <returns></returns>
        public SkillResponse FunctionHandler(SkillRequest input, ILambdaContext ctx)
        {
            _context = ctx;
            try
            {
                _response = new SkillResponse
                {
                    Response = new ResponseBody { ShouldEndSession = false },
                    Version = AlexaConstants.AlexaVersion
                };

                if (input.Request.Type.Equals(AlexaConstants.LaunchRequest))
                {
                    string locale = input.Request.Locale;
                    if (string.IsNullOrEmpty(locale))
                    {
                        locale = SpainLocale;
                    }

                    var lineData = InitializeData();
                    ProcessLaunchRequest(lineData, _response.Response);
                    _response.SessionAttributes = new Dictionary<string, object>() { { Localename, locale } };
                }
                else
                {
                    if (input.Request.Type.Equals(AlexaConstants.IntentRequest))
                    {
                        _sender = new MessageSender();
                        _sender.ConnectWithMqttBroker();
                        string locale = string.Empty;
                        Dictionary<string, object> dictionary = input.Session.Attributes;
                        if (dictionary != null)
                        {
                            if (dictionary.ContainsKey(Localename))
                            {
                                locale = (string)dictionary[Localename];
                            }
                        }
                        if (string.IsNullOrEmpty(locale))
                        {
                            locale = input.Request.Locale;
                        }
                        if (string.IsNullOrEmpty(locale))
                        {
                            locale = SpainLocale;
                        }
                        _response.SessionAttributes = new Dictionary<string, object>() { { Localename, locale } };
                        var lineData = InitializeData();
                        if (IsDialogIntentRequest(input))
                        {
                            if (!IsDialogSequenceComplete(input))
                            {
                                CreateDelegateResponse();
                                return _response;
                            }
                        }
                        if (!ProcessDialogRequest(lineData, input, _response))
                        {
                            _response.Response.OutputSpeech = ProcessIntentRequest(lineData, input);
                        }
                    }
                }
                Log(JsonConvert.SerializeObject(_response));
                return _response;
            }
            catch (Exception ex)
            {
                Log($"error :" + ex.Message);
                _response.Response.OutputSpeech = RespondExceptionError(ex.Message);
            }
            return _response;
        }

        /// <summary>
        /// Process and respond to the LaunchRequest with launch
        /// and reprompt message
        /// </summary>
        /// <param name="factdata">The factdata.</param>
        /// <param name="response">The response.</param>
        private void ProcessLaunchRequest(LineData factdata, ResponseBody response)
        {
            if (factdata == null) return;
            IOutputSpeech innerResponse = new SsmlOutputSpeech();
            ((SsmlOutputSpeech)innerResponse).Ssml = SsmlDecorate(factdata.LaunchMessage);
            response.OutputSpeech = innerResponse;
            IOutputSpeech prompt = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech)prompt).Text = factdata.LaunchMessageReprompt;
            response.Reprompt = new Reprompt()
            {
                OutputSpeech = prompt
            };
        }

        private void ProcessEndRequest(LineData factdata, ResponseBody response)
        {
            if (factdata == null) return;
            IOutputSpeech innerResponse = new SsmlOutputSpeech();
            ((SsmlOutputSpeech)innerResponse).Ssml = SsmlDecorate(factdata.StopMessage);
            response.OutputSpeech = innerResponse;
            IOutputSpeech prompt = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech)prompt).Text = factdata.LaunchMessageReprompt;
            response.Reprompt = new Reprompt()
            {
                OutputSpeech = prompt
            };
        }

        /// <summary>
        /// Check if its IsDialogIntentRequest, e.g. part of a Dialog sequence
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        /// bool true if a dialog
        /// </returns>
        private bool IsDialogIntentRequest(SkillRequest input)
        {
            if (string.IsNullOrEmpty(input.Request.DialogState))
                return false;
            return true;
        }

        /// <summary>
        /// Determines whether [is dialog sequence complete] [the specified input].
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if [is dialog sequence complete] [the specified input]; otherwise, <c>false</c>.
        /// </returns>
        private bool IsDialogSequenceComplete(SkillRequest input)
        {
            if (input.Request.DialogState.Equals(AlexaConstants.DialogStarted)
               || input.Request.DialogState.Equals(AlexaConstants.DialogInProgress))
            {
                return false;
            }
            if (input.Request.DialogState.Equals(AlexaConstants.DialogCompleted))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Processes the dialog request.
        /// </summary>
        /// <param name="factdata">The factdata.</param>
        /// <param name="input">The input.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        private bool ProcessDialogRequest(LineData factdata, SkillRequest input, SkillResponse response)
        {
            var intentRequest = input.Request;
            bool processed = false;

            switch (intentRequest.Intent.Name)
            {
                case "Arranca":
                case "Para":
                    var statusMessage = SetMachineStatus(intentRequest.Intent.Name.Equals("Arranca"), factdata, intentRequest);
                    if (!string.IsNullOrEmpty(statusMessage))
                    {
                        response.Response.OutputSpeech = new SsmlOutputSpeech();
                        ((SsmlOutputSpeech)response.Response.OutputSpeech).Ssml = SsmlDecorate(statusMessage);
                    }
                    processed = true;
                    break;
                case "AumentaVelocidad":
                case "DisminuyeVelocidad":
                    var velocityMessage = SetMachineVelocity(intentRequest.Intent.Name, factdata, intentRequest);
                    if (!string.IsNullOrEmpty(velocityMessage))
                    {
                        response.Response.OutputSpeech = new SsmlOutputSpeech();
                        ((SsmlOutputSpeech)response.Response.OutputSpeech).Ssml = SsmlDecorate(velocityMessage);
                    }
                    processed = true;
                    break;
                case "AnyadeUnidades":
                    var anyadeMessage = AddUnitToWorkOrder(intentRequest.Intent.Name, factdata, intentRequest);
                    if (!string.IsNullOrEmpty(anyadeMessage))
                    {
                        response.Response.OutputSpeech = new SsmlOutputSpeech();
                        ((SsmlOutputSpeech)response.Response.OutputSpeech).Ssml = SsmlDecorate(anyadeMessage);
                    }
                    processed = true;
                    break;
                case "AnyadeMedicion":
                    var anyadeMedicion = AddMeasurementToWorkOrder(intentRequest.Intent.Name, factdata, intentRequest);
                    if (!string.IsNullOrEmpty(anyadeMedicion))
                    {
                        response.Response.OutputSpeech = new SsmlOutputSpeech();
                        ((SsmlOutputSpeech)response.Response.OutputSpeech).Ssml = SsmlDecorate(anyadeMedicion);
                    }
                    processed = true;
                    break;
                case "AccionSobreOrdenTrabajo":
                    var accionSobreOrdenTrabajo = AddActionOverWorkOrder(intentRequest.Intent.Name, factdata, intentRequest);
                    if (!string.IsNullOrEmpty(accionSobreOrdenTrabajo))
                    {
                        response.Response.OutputSpeech = new SsmlOutputSpeech();
                        ((SsmlOutputSpeech)response.Response.OutputSpeech).Ssml = SsmlDecorate(accionSobreOrdenTrabajo);
                    }
                    processed = true;
                    break;
            }

            return processed;
        }

        /// <summary>
        /// prepare text for Ssml display
        /// </summary>
        /// <param name="speech">The speech.</param>
        /// <returns>
        /// string
        /// </returns>
        private string SsmlDecorate(string speech)
        {
            return "<speak>" + speech + "</speak>";
        }

        /// <summary>
        /// Process all not dialog based Intents
        /// </summary>
        /// <param name="factdata">The factdata.</param>
        /// <param name="input">The input.</param>
        /// <returns>
        /// IOutputSpeech innerResponse
        /// </returns>
        private IOutputSpeech ProcessIntentRequest(LineData factdata, SkillRequest input)
        {
            var intentRequest = input.Request;
            IOutputSpeech innerResponse = new PlainTextOutputSpeech();

            switch (intentRequest.Intent.Name)
            {
                case AlexaConstants.CancelIntent:
                    ((PlainTextOutputSpeech)innerResponse).Text = factdata.StopMessage;
                    _response.Response.ShouldEndSession = true;
                    break;

                case AlexaConstants.StopIntent:
                    ((PlainTextOutputSpeech)innerResponse).Text = factdata.StopMessage;
                    _response.Response.ShouldEndSession = true;
                    break;

                case AlexaConstants.HelpIntent:
                    ((PlainTextOutputSpeech)innerResponse).Text = factdata.HelpMessage;
                    break;

                default:
                    ((PlainTextOutputSpeech)innerResponse).Text = factdata.HelpReprompt;
                    break;
            }
            if (innerResponse.Type == AlexaConstants.SSMLSpeech)
            {
                BuildCard(factdata.SkillName, (innerResponse as SsmlOutputSpeech).Ssml);
                (innerResponse as SsmlOutputSpeech).Ssml = SsmlDecorate((innerResponse as SsmlOutputSpeech).Ssml);
            }
            return innerResponse;
        }

        /// <summary>
        /// Responds the exception error.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <returns></returns>
        private IOutputSpeech RespondExceptionError(string error)
        {
            IOutputSpeech innerResponse = new PlainTextOutputSpeech();
            ((PlainTextOutputSpeech)innerResponse).Text = error;
            if (innerResponse.Type == AlexaConstants.SSMLSpeech)
            {
                BuildCard("machine", (innerResponse as SsmlOutputSpeech).Ssml);
                (innerResponse as SsmlOutputSpeech).Ssml = SsmlDecorate((innerResponse as SsmlOutputSpeech).Ssml);
            }
            return innerResponse;
        }

        /// <summary>
        /// Build a simple card, setting its title and content field
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="output">The output.</param>
        private void BuildCard(string title, string output)
        {
            if (!string.IsNullOrEmpty(output))
            {
                output = Regex.Replace(output, @"<.*?>", "");
                _response.Response.Card = new SimpleCard()
                {
                    Title = title,
                    Content = output,
                };
            }
        }

        /// <summary>
        /// Sets the machine status.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="factdata">The factdata.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private string SetMachineVelocity(string action, LineData factdata, Request request)
        {
            try
            {
                if (request.Intent.Slots.ContainsKey("Line") && request.Intent.Slots.ContainsKey("Velocidad"))
                {
                    if (request.Intent.Slots.TryGetValue("Line", out var slot)
                        && request.Intent.Slots.TryGetValue("Velocidad", out var velocidad)
                        && int.TryParse(velocidad.Value, out var vel))
                    {
                        if (_sender.SendLineVelocityMessage(slot.Value.ToLower(), vel, action))
                        {
                            string actionText = action.Equals("AumentaVelocidad") ? "aumentado" : "disminuido";
                            return $"La velocidad de la linea {slot.Value} se ha {action} en {vel} unidades" + factdata.AskMessage;
                        }
                        return InvalidSlotMessage(_lineData);
                    }
                }
                return InvalidSlotMessage(_lineData);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Sets the machine status.
        /// </summary>
        /// <param name="status">if set to <c>true</c> [status].</param>
        /// <param name="factdata">The factdata.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private string SetMachineStatus(bool status, LineData factdata, Request request)
        {
            try
            {
                if (request.Intent.Slots.ContainsKey("Line"))
                {
                    if (request.Intent.Slots.TryGetValue("Line", out var slot))
                    {
                        if (_sender.SendLineStatusMessage(slot.Value.ToLower(), status))
                        {
                            var linestatus = status ? "Arrancado" : "Parado";
                            return $"La Linea {slot.Value} se ha {linestatus} correctamente " + factdata.AskMessage;
                        }
                        return InvalidSlotMessage(_lineData);
                    }
                }

                return InvalidSlotMessage(_lineData); ;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Adds the unit to work order.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="factdata">The factdata.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private string AddUnitToWorkOrder(string action, LineData factdata, Request request)
        {
            try
            {
                if (request.Intent.Slots.ContainsKey("OrdenTrabajo") && request.Intent.Slots.ContainsKey("Unidades"))
                {
                    if (request.Intent.Slots.TryGetValue("OrdenTrabajo", out var slotOrd)
                        && request.Intent.Slots.TryGetValue("Unidades", out var slotUnit)
                        && int.TryParse(slotOrd.Value, out var order) && int.TryParse(slotUnit.Value, out var unit))
                    {
                        if (_sender.SendAddUnitToWorkOrder(order, unit, action))
                        {
                            return $"Se han a�adido {unit} unidades a la orden de trabajo {order} " + factdata.AskMessage;
                        }
                        return InvalidSlotMessage(_lineData);
                    }
                }
                return InvalidSlotMessage(_lineData);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// Adds the measurement to work order.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="factdata">The factdata.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private string AddMeasurementToWorkOrder(string action, LineData factdata, Request request)
        {
            try
            {
                if (request.Intent.Slots.ContainsKey("OrdenTrabajo") && request.Intent.Slots.ContainsKey("Medicion"))
                {
                    if (request.Intent.Slots.TryGetValue("OrdenTrabajo", out var slotOrd)
                        && request.Intent.Slots.TryGetValue("Medicion", out var slotMedicion)
                        && int.TryParse(slotOrd.Value, out var order))
                    {
                        if (_sender.SendAddMeasurementToWorkOrder(order, slotMedicion.Value))
                        {
                            return $"Se ha a�adido la medici�n {slotMedicion} a la orden de trabajo {order} " + factdata.AskMessage;
                        }
                        return InvalidSlotMessage(_lineData);
                    }
                }
                return InvalidSlotMessage(_lineData);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// AddActionOverWorkOrder
        /// </summary>
        /// <param name="name"></param>
        /// <param name="factdata"></param>
        /// <param name="intentRequest"></param>
        /// <returns></returns>
        private string AddActionOverWorkOrder(string name, LineData factdata, Request request)
        {
            try
            {
                if (request.Intent.Slots.ContainsKey("OrdenTrabajo") && request.Intent.Slots.ContainsKey("Accion") && request.Intent.Slots.ContainsKey("Line"))
                {
                    if (request.Intent.Slots.TryGetValue("OrdenTrabajo", out var slotOrd)
                        && request.Intent.Slots.TryGetValue("Line", out var slotLine)
                        && request.Intent.Slots.TryGetValue("Accion", out var slotAccion)
                        && int.TryParse(slotOrd.Value, out var order))
                    {
                        if (_sender.SendAddActionOverWorkOrder(slotAccion.Value, order.ToString(), slotLine.Value))
                        {
                            string slotActionText = string.Empty;
                            switch (slotAccion.Value)
                            {
                                case "Arranca":
                                    slotActionText = "arranca";
                                    break;
                                case "Pausa":
                                    slotActionText = "pausa";
                                    break;
                                case "Reanuda":
                                    slotActionText = "reanuda";
                                    break;
                                case "Termina":
                                    slotActionText = "termina";
                                    break;
                            }
                            return $"Se ha enviado la acci�n {slotActionText} sobre la orden de trabajo {order} en la l�nea {slotLine.Value}" + factdata.AskMessage;
                        }
                        return InvalidSlotMessage(_lineData);
                    }
                }
                return InvalidSlotMessage(_lineData);
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// create a delegate response, we delegate all the dialog requests
        /// except "Complete"
        /// </summary>
        private void CreateDelegateResponse()
        {
            DialogDirective dld = new DialogDirective()
            {
                Type = AlexaConstants.DialogDelegate
            };
            _response.Response.Directives.Add(dld);
        }

        /// <summary>
        /// create a invalid slot value message
        /// </summary>
        /// <param name="factdata">The factdata.</param>
        /// <returns>
        /// string invalid planet name or empty string
        /// </returns>
        private string InvalidSlotMessage(LineData factdata)
        {
            var output = "La instrucion no se ha procesado" + factdata.HelpMessage;
            return output;
        }

        /// <summary>
        /// Get the fatcs and questions for the specified locale
        /// </summary>
        /// <returns>
        /// factdata for the locale
        /// </returns>
        private LineData InitializeData()
        {
            return _lineData ?? (_lineData = LineData.LoadDefaultMessage());
        }

        /// <summary>
        /// logger interface
        /// </summary>
        /// <param name="text">The text.</param>
        private void Log(string text)
        {
            _context?.Logger.LogLine(text);
        }
    }
}
