﻿using System.Collections.Generic;

namespace lambda
{
    public class LineData
    {
        /// <summary>
        /// Holds locale based arrays of space facts and other information
        /// </summary>
        /// <param name="locale"></param>
        /// <returns>factdata for the locale</returns>
        public LineData(string locale)
        {
            Locale = locale;
        }

        public string Locale { get; set; }
        public string SkillName { get; set; }
        public string HelpMessage { get; set; }
        public string HelpReprompt { get; set; }
        public string StopMessage { get; set; }
        public string LaunchMessage { get; set; }
        public string LaunchMessageReprompt { get; set; }
        public string AskMessage { get; set; }

        /// <summary>
        ///  Load the resources into the resourse array for processing
        /// </summary>
        /// <returns>void</returns>
        public static LineData LoadDefaultMessage()
        {
            LineData lineData = new LineData("es-ES")
            {
                SkillName = "Gestion Linea produccion",
                HelpMessage = "¿En que puedo ayudarle?",
                HelpReprompt = "¿En que puedo ayudarle?",
                StopMessage = "Skill Desactivada",
                LaunchMessage = "Buenos dias, la aplícacion de gestión Voice To Factory se ha cargado correctamente, ¿Que acción desea realizar?",
                LaunchMessageReprompt = "Pruebe de nuevo .",
                AskMessage = " ¿En que mas puedo ayudarle?"
            };
            return lineData;
        }
    }
}
