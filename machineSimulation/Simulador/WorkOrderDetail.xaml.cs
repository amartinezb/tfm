﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using machineSimulation;

namespace Simulador
{
    /// <summary>
    /// Interaction logic for WorkOrderDetail.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class WorkOrderDetail : Window
    {
        /// <summary>
        /// The work order
        /// </summary>
        private WorkOrder _workOrder;

        /// <summary>
        /// Gets the work order identifier.
        /// </summary>
        /// <value>
        /// The work order identifier.
        /// </value>
        public string WorkOrderId => _workOrder.WorkOrderId.ToString();

        /// <summary>
        /// Gets the total units.
        /// </summary>
        /// <value>
        /// The total units.
        /// </value>
        public string TotalUnits => _workOrder.TotalUnits.ToString();

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status => _workOrder.Status.ToString();

        /// <summary>
        /// Gets the produced units.
        /// </summary>
        /// <value>
        /// The produced units.
        /// </value>
        public string ProducedUnits => _workOrder.CurrentProducedUnit.ToString();

        /// <summary>
        /// Gets the annotations.
        /// </summary>
        /// <value>
        /// The annotations.
        /// </value>
        public string Annotations => string.Join("|", _workOrder.Anotations.Select(x => x.Key + ":" + x.Value));


        /// <summary>
        /// Initializes a new instance of the <see cref="WorkOrderDetail"/> class.
        /// </summary>
        /// <param name="workOrder">The work order.</param>
        public WorkOrderDetail(WorkOrder workOrder)
        {
            this._workOrder = workOrder;
            InitializeComponent();
            Loaded += Window_Loaded;
        }

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;
        }
    }
}
