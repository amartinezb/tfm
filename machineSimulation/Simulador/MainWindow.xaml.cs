﻿using System;
using System.ComponentModel;
using System.Security.Cryptography.X509Certificates;
using System.Speech.Recognition;
using System.Speech.Synthesis;
using System.Windows;
using machineSimulation;
using Newtonsoft.Json;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Simulador
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <seealso cref="System.Windows.Window" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// The connected line1
        /// </summary>
        private bool _connectedLine1;
        /// <summary>
        /// The connected line2
        /// </summary>
        private bool _connectedLine2;
        /// <summary>
        /// The connected line3
        /// </summary>
        private bool _connectedLine3;
        /// <summary>
        /// The connected line4
        /// </summary>
        private bool _connectedLine4;
        /// <summary>
        /// The velocity line1
        /// </summary>
        private int _velocityLine1;
        /// <summary>
        /// The velocity line2
        /// </summary>
        private int _velocityLine2;
        /// <summary>
        /// The velocity line3
        /// </summary>
        private int _velocityLine3;
        /// <summary>
        /// The velocity line4
        /// </summary>
        private int _velocityLine4;
        /// <summary>
        /// The wo line1
        /// </summary>
        private int _woLine1;
        /// <summary>
        /// The wo line2
        /// </summary>
        private int _woLine2;
        /// <summary>
        /// The wo line3
        /// </summary>
        private int _woLine3;
        /// <summary>
        /// The wo line4
        /// </summary>
        private int _woLine4;
        /// <summary>
        /// The temperature line1
        /// </summary>
        private float _temperatureLine1;
        /// <summary>
        /// The temperature line2
        /// </summary>
        private float _temperatureLine2;
        /// <summary>
        /// The temperature line3
        /// </summary>
        private float _temperatureLine3;
        /// <summary>
        /// The temperature line4
        /// </summary>
        private float _temperatureLine4;
        /// <summary>
        /// The coil line1
        /// </summary>
        private int _coilLine1;
        /// <summary>
        /// The coil line2
        /// </summary>
        private int _coilLine2;
        /// <summary>
        /// The coil line3
        /// </summary>
        private int _coilLine3;
        /// <summary>
        /// The coil line4
        /// </summary>
        private int _coilLine4;
        /// <summary>
        /// The blade1
        /// </summary>
        private int _blade1;
        /// <summary>
        /// The blade2
        /// </summary>
        private int _blade2;
        /// <summary>
        /// The blade3
        /// </summary>
        private int _blade3;
        /// <summary>
        /// The blade4
        /// </summary>
        private int _blade4;
        /// <summary>
        /// The work order line1
        /// </summary>
        private WorkOrder WorkOrderLine1;
        /// <summary>
        /// The work order line2
        /// </summary>
        private WorkOrder WorkOrderLine2;
        /// <summary>
        /// The work order line3
        /// </summary>
        private WorkOrder WorkOrderLine3;
        /// <summary>
        /// The work order line4
        /// </summary>
        private WorkOrder WorkOrderLine4;

        /// <summary>
        /// Gets or sets the status line1.
        /// </summary>
        /// <value>
        /// The status line1.
        /// </value>
        public string StatusLine1
        {
            get => _connectedLine1 ? "Arrancada" : "Parada";
            set
            {
                _connectedLine1 = value.Equals("True");
                NotifyPropertyChanged(nameof(StatusLine1));
            }
        }

        /// <summary>
        /// Gets or sets the status line2.
        /// </summary>
        /// <value>
        /// The status line2.
        /// </value>
        public string StatusLine2
        {
            get => _connectedLine2 ? "Arrancada" : "Parada";
            set
            {
                _connectedLine2 = value.Equals("True");
                NotifyPropertyChanged(nameof(StatusLine2));
            }
        }

        /// <summary>
        /// Gets or sets the status line3.
        /// </summary>
        /// <value>
        /// The status line3.
        /// </value>
        public string StatusLine3
        {
            get => _connectedLine3 ? "Arrancada" : "Parada";
            set
            {
                _connectedLine3 = value.Equals("True");
                NotifyPropertyChanged(nameof(StatusLine3));
            }
        }

        /// <summary>
        /// Gets or sets the status line4.
        /// </summary>
        /// <value>
        /// The status line4.
        /// </value>
        public string StatusLine4
        {
            get => _connectedLine4 ? "Arrancada" : "Parada";
            set
            {
                _connectedLine4 = value.Equals("True");
                NotifyPropertyChanged(nameof(StatusLine4));
            }
        }

        /// <summary>
        /// Gets or sets the velocity line1.
        /// </summary>
        /// <value>
        /// The velocity line1.
        /// </value>
        public int VelocityLine1
        {
            get => _velocityLine1;
            set
            {
                _velocityLine1 = value;
                NotifyPropertyChanged(nameof(VelocityLine1));
            }
        }

        /// <summary>
        /// Gets or sets the velocity line2.
        /// </summary>
        /// <value>
        /// The velocity line2.
        /// </value>
        public int VelocityLine2
        {
            get { return _velocityLine2; }
            set
            {
                _velocityLine2 = value;
                NotifyPropertyChanged(nameof(VelocityLine2));
            }
        }

        /// <summary>
        /// Gets or sets the velocity line3.
        /// </summary>
        /// <value>
        /// The velocity line3.
        /// </value>
        public int VelocityLine3
        {
            get { return _velocityLine3; }
            set
            {
                _velocityLine3 = value;
                NotifyPropertyChanged(nameof(VelocityLine3));
            }
        }

        /// <summary>
        /// Gets or sets the velocity line4.
        /// </summary>
        /// <value>
        /// The velocity line4.
        /// </value>
        public int VelocityLine4
        {
            get { return _velocityLine4; }
            set
            {
                _velocityLine4 = value;
                NotifyPropertyChanged(nameof(VelocityLine4));
            }
        }

        /// <summary>
        /// Gets or sets the temperature line1.
        /// </summary>
        /// <value>
        /// The temperature line1.
        /// </value>
        public float TemperatureLine1
        {
            get => _temperatureLine1;
            set
            {
                _temperatureLine1 = value;
                NotifyPropertyChanged(nameof(TemperatureLine1));
            }
        }

        /// <summary>
        /// Gets or sets the temperature line2.
        /// </summary>
        /// <value>
        /// The temperature line2.
        /// </value>
        public float TemperatureLine2
        {
            get => _temperatureLine2;
            set
            {
                _temperatureLine2 = value;
                NotifyPropertyChanged(nameof(TemperatureLine2));
            }
        }

        /// <summary>
        /// Gets or sets the temperature line3.
        /// </summary>
        /// <value>
        /// The temperature line3.
        /// </value>
        public float TemperatureLine3
        {
            get => _temperatureLine3;
            set
            {
                _temperatureLine3 = value;
                NotifyPropertyChanged(nameof(TemperatureLine3));
            }
        }

        /// <summary>
        /// Gets or sets the temperature line4.
        /// </summary>
        /// <value>
        /// The temperature line4.
        /// </value>
        public float TemperatureLine4
        {
            get => _temperatureLine4;
            set
            {
                _temperatureLine4 = value;
                NotifyPropertyChanged(nameof(TemperatureLine4));
            }
        }

        /// <summary>
        /// Gets or sets the sharp blade line1.
        /// </summary>
        /// <value>
        /// The sharp blade line1.
        /// </value>
        public int SharpBladeLine1
        {
            get => _blade1;
            set
            {
                _blade1 = value;
                NotifyPropertyChanged(nameof(SharpBladeLine1));
            }
        }

        /// <summary>
        /// Gets or sets the sharp blade line2.
        /// </summary>
        /// <value>
        /// The sharp blade line2.
        /// </value>
        public int SharpBladeLine2
        {
            get => _blade2;
            set
            {
                _blade2 = value;
                NotifyPropertyChanged(nameof(SharpBladeLine2));
            }
        }

        /// <summary>
        /// Gets or sets the sharp blade line3.
        /// </summary>
        /// <value>
        /// The sharp blade line3.
        /// </value>
        public int SharpBladeLine3
        {
            get => _blade3;
            set
            {
                _blade3 = value;
                NotifyPropertyChanged(nameof(SharpBladeLine3));
            }
        }

        /// <summary>
        /// Gets or sets the sharp blade line4.
        /// </summary>
        /// <value>
        /// The sharp blade line4.
        /// </value>
        public int SharpBladeLine4
        {
            get => _blade4;
            set
            {
                _blade4 = value;
                NotifyPropertyChanged(nameof(SharpBladeLine4));
            }
        }

        /// <summary>
        /// Gets or sets the coil status line1.
        /// </summary>
        /// <value>
        /// The coil status line1.
        /// </value>
        public int CoilStatusLine1
        {
            get => _coilLine1;
            set
            {
                _coilLine1 = value;
                NotifyPropertyChanged(nameof(CoilStatusLine1));
            }
        }

        /// <summary>
        /// Gets or sets the coil status line2.
        /// </summary>
        /// <value>
        /// The coil status line2.
        /// </value>
        public int CoilStatusLine2
        {
            get => _coilLine2;
            set
            {
                _coilLine2 = value;
                NotifyPropertyChanged(nameof(CoilStatusLine2));
            }
        }

        /// <summary>
        /// Gets or sets the coil status line3.
        /// </summary>
        /// <value>
        /// The coil status line3.
        /// </value>
        public int CoilStatusLine3
        {
            get => _coilLine3;
            set
            {
                _coilLine3= value;
                NotifyPropertyChanged(nameof(CoilStatusLine3));
            }
        }

        /// <summary>
        /// Gets or sets the coil status line4.
        /// </summary>
        /// <value>
        /// The coil status line4.
        /// </value>
        public int CoilStatusLine4
        {
            get => _coilLine4;
            set
            {
                _coilLine4 = value;
                NotifyPropertyChanged(nameof(CoilStatusLine4));
            }
        }

        /// <summary>
        /// Gets or sets the work order identifier line1.
        /// </summary>
        /// <value>
        /// The work order identifier line1.
        /// </value>
        public int WorkOrderIdLine1
        {
            get => _woLine1;
            set
            {
                _woLine1 = value;
                NotifyPropertyChanged(nameof(WorkOrderIdLine1));
            }
        }

        /// <summary>
        /// Gets or sets the work order identifier line2.
        /// </summary>
        /// <value>
        /// The work order identifier line2.
        /// </value>
        public int WorkOrderIdLine2
        {
            get => _woLine2;
            set
            {
                _woLine2 = value;
                NotifyPropertyChanged(nameof(WorkOrderIdLine2));
            }
        }

        /// <summary>
        /// Gets or sets the work order identifier line3.
        /// </summary>
        /// <value>
        /// The work order identifier line3.
        /// </value>
        public int WorkOrderIdLine3
        {
            get => _woLine3;
            set
            {
                _woLine3 = value;
                NotifyPropertyChanged(nameof(WorkOrderIdLine3));
            }
        }

        /// <summary>
        /// Gets or sets the work order identifier line4.
        /// </summary>
        /// <value>
        /// The work order identifier line4.
        /// </value>
        public int WorkOrderIdLine4
        {
            get => _woLine4;
            set
            {
                _woLine4 = value;
                NotifyPropertyChanged(nameof(WorkOrderIdLine4));
            }
        }

        /// <summary>
        /// The speech synthesizer
        /// </summary>
        SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer
        {
            Volume = 100,
            Rate = 2
        };

        /// <summary>
        /// The s recognize
        /// </summary>
        SpeechRecognitionEngine sRecognize = new SpeechRecognitionEngine();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Loaded += Window_Loaded;
        }

        /// <summary>
        /// Handles the Loaded event of the Window control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this;
            ConsumeLineMessages();
        }

        // implement the INotify
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        /// <summary>
        /// Consumes the line messages.
        /// </summary>
        public void ConsumeLineMessages()
        {
            try
            {
                string iotendpoint = "a2dqsjwbyvfvwy-ats.iot.us-east-2.amazonaws.com";
                int BrokerPort = 8883;
                string Topic = "Lines/Measurements";
                var CaCert = X509Certificate.CreateFromCertFile(@".\Certificates\root-CA.crt");
                var ClientCert = new X509Certificate2(@".\Certificates\dotnet_devicecertificate.pfx", "uranio32");
                string ClientID = Guid.NewGuid().ToString();
                var IotClient = new MqttClient(iotendpoint, BrokerPort, true, CaCert, ClientCert,
                    MqttSslProtocols.TLSv1_2);
                IotClient.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                IotClient.MqttMsgSubscribed += Client_MqttMsgSubscribed;
                IotClient.Connect(ClientID);
                IotClient.Subscribe(new string[] { Topic }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Handles the MqttMsgSubscribed event of the Client control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MqttMsgSubscribedEventArgs"/> instance containing the event data.</param>
        private void Client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {

        }

        /// <summary>
        /// Handles the MqttMsgPublishReceived event of the Client control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MqttMsgPublishEventArgs"/> instance containing the event data.</param>
        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                sRecognize.SpeechRecognized -= SRecognize_SpeechRecognized;
                string message = System.Text.Encoding.UTF8.GetString(e.Message);
                ReportStatus reportStatus = JsonConvert.DeserializeObject<ReportStatus>(message);
                switch (reportStatus.Line.LineIdentifier)
                {
                    case "A":
                    case "a":
                        VelocityLine1 = reportStatus.Line.LineVelocity;
                        StatusLine1 = reportStatus?.Line?.LineWorking.ToString();
                        CoilStatusLine1 = reportStatus.Line.PaperCoilPercentage;
                        SharpBladeLine1 = reportStatus.Line.SharpBladePercentage;
                        TemperatureLine1 = reportStatus.Line.LineTemperature;
                        if (reportStatus.Line.CurrentWorkOrder != null)
                        {
                            WorkOrderLine1 = reportStatus.Line.CurrentWorkOrder;
                            WorkOrderIdLine1 = reportStatus.Line.CurrentWorkOrder.WorkOrderId;
                        }
                        else
                        {
                            WorkOrderIdLine1 = 0;
                            WorkOrderLine1 = null;
                        }

                        SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer
                        {
                            Volume = 100,
                            Rate = 2
                        };
                        speechSynthesizer.SelectVoiceByHints(VoiceGender.Female);

                        speechSynthesizer.Speak("Llego reporte periodico de la Linea A");

                        if (reportStatus.Line.PaperCoilPercentage < 20)
                        {
                            speechSynthesizer.Speak("La bobina de la Linea A esta apunto de acabarse, se sugiere cargar un nuevo rollo de materia prima");
                        }
                        if (reportStatus.Line.LineTemperature > 100)
                        {
                            speechSynthesizer.Speak($"La temperatura de la maquina de la linea A  es demasiado alta, es de {reportStatus.Line.LineTemperature}, se sugiere disminuir la velocidad de producción");
                        }

                        if (reportStatus.Line.SharpBladePercentage < 20)
                        {
                            speechSynthesizer.Speak("La cuchilla de la Linea A esta debajo del minimo de filo, se sugiere reemplazarla");
                        }
                        speechSynthesizer.Speak("¿Desea escuchar el reporte de la linea A?");

                        Choices sList = new Choices();
                        sList.Add(new String[] { "Si" });
                        Grammar gr = new Grammar(new GrammarBuilder(sList));
                        sRecognize = new SpeechRecognitionEngine();
                        sRecognize.RequestRecognizerUpdate();
                        sRecognize.LoadGrammar(gr);
                        sRecognize.SpeechRecognized += SRecognize_SpeechRecognized;
                        sRecognize.SetInputToDefaultAudioDevice();
                        sRecognize.RecognizeAsync(RecognizeMode.Single);

                        break;
                    case "B":
                    case "b":
                        StatusLine2 = reportStatus.Line.LineWorking.ToString();
                        VelocityLine2 = reportStatus.Line.LineVelocity;
                        if (reportStatus.Line.CurrentWorkOrder != null)
                        {
                            WorkOrderLine2 = reportStatus.Line.CurrentWorkOrder;
                            WorkOrderIdLine2 = reportStatus.Line.CurrentWorkOrder.WorkOrderId;
                        }
                        else
                        {
                            WorkOrderIdLine2 = 0;
                            WorkOrderLine2 = null;
                        }
                        break;
                    case "C":
                    case "c":
                        StatusLine3 = reportStatus.Line.LineWorking.ToString();
                        VelocityLine3 = reportStatus.Line.LineVelocity;
                        if (reportStatus.Line.CurrentWorkOrder != null)
                        {
                            WorkOrderLine3 = reportStatus.Line.CurrentWorkOrder;
                            WorkOrderIdLine3 = reportStatus.Line.CurrentWorkOrder.WorkOrderId;
                        }
                        else
                        {
                            WorkOrderIdLine3 = 0;
                            WorkOrderLine3 = null;
                        }
                        break;
                    case "D":
                    case "d":
                        StatusLine4 = reportStatus.Line.LineWorking.ToString();
                        VelocityLine4 = reportStatus.Line.LineVelocity;
                        if (reportStatus.Line.CurrentWorkOrder != null)
                        {
                            WorkOrderLine4 = reportStatus.Line.CurrentWorkOrder;
                            WorkOrderIdLine4 = reportStatus.Line.CurrentWorkOrder.WorkOrderId;
                        }
                        else
                        {
                            WorkOrderIdLine4 = 0;
                            WorkOrderLine4 = null;
                        }
                        break;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }

        }

        /// <summary>
        /// Handles the SpeechRecognized event of the SRecognize control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Speech.Recognition.SpeechRecognizedEventArgs" /> instance containing the event data.</param>
        private void SRecognize_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer();
            speechSynthesizer.Volume = 100;
            speechSynthesizer.Rate = 2;
            speechSynthesizer.SelectVoiceByHints(VoiceGender.Female);
            speechSynthesizer.Speak($"La linea A esta {StatusLine1}");
            speechSynthesizer.Speak($"La velocidad configurada en la linea A es {VelocityLine1}");
            speechSynthesizer.Speak($"La temperatura de la maquina es {TemperatureLine1}");
            speechSynthesizer.Speak($"El porcentaje de afilado de la cuchilla es {SharpBladeLine1}");
            speechSynthesizer.Speak($"El porcentaje de la bobina de materia prima es {CoilStatusLine1}");

            if (WorkOrderLine1 != null)
            {
                speechSynthesizer.Speak($"La linea esta produciendo la orden de trabajo {WorkOrderLine1.WorkOrderId}");
            }
            else
            {
                speechSynthesizer.Speak($"La linea no esta produciendo ninguna orden de trabajo");
            }
        }

        /// <summary>
        /// Handles the OnClick event of the ButtonWorkOrder1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ButtonWorkOrder1_OnClick(object sender, RoutedEventArgs e)
        {
            if (WorkOrderLine1 != null)
            {
                WorkOrderDetail detail = new WorkOrderDetail(WorkOrderLine1);
                detail.Show();
            }
        }

        /// <summary>
        /// Handles the OnClick event of the ButtonWorkOrder2 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ButtonWorkOrder2_OnClick(object sender, RoutedEventArgs e)
        {
            if (WorkOrderLine2 != null)
            {
                WorkOrderDetail detail = new WorkOrderDetail(WorkOrderLine2);
                detail.Show();
            }
        }

        /// <summary>
        /// Handles the OnClick event of the ButtonWorkOrder3 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ButtonWorkOrder3_OnClick(object sender, RoutedEventArgs e)
        {
            if (WorkOrderLine3 != null)
            {
                WorkOrderDetail detail = new WorkOrderDetail(WorkOrderLine3);
                detail.Show();
            }
        }

        /// <summary>
        /// Handles the OnClick event of the ButtonWorkOrder4 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void ButtonWorkOrder4_OnClick(object sender, RoutedEventArgs e)
        {
            if (WorkOrderLine4 != null)
            {
                WorkOrderDetail detail = new WorkOrderDetail(WorkOrderLine4);
                detail.Show();
            }
        }
    }
}
