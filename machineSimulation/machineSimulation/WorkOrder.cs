﻿using System;
using System.Collections.Generic;
using System.Text;

namespace machineSimulation
{
    public class WorkOrder
    {
        public int WorkOrderId;

        public WorkOrderStatus Status;

        public Dictionary<string,string> Anotations;

        public int CurrentProducedUnit;

        public int TotalUnits;

        public WorkOrder(int workOrderId, int totalUnits)
        {
            Anotations = new Dictionary<string, string>();
            CurrentProducedUnit = 0;
            TotalUnits = totalUnits;
            WorkOrderId = workOrderId;
            Status = WorkOrderStatus.Parada;
        }

        public void AddAnotation(string key, string value)=> Anotations.Add(key,value);
        
        public void AddUnitToWorkOrder(int unitsToAdd) => TotalUnits = TotalUnits + unitsToAdd;
        
        public void ChangeWorkOrderStatus(WorkOrderStatus status) => Status = status;
    }

    public  enum WorkOrderStatus
    {
        Iniciada,
        Pausada,
        Parada
    }
}
