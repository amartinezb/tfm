﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Newtonsoft.Json;
using uPLibrary.Networking.M2Mqtt;

namespace machineSimulation
{
        public class ReportStatus
    {
        const string IotEndpoint = "a2dqsjwbyvfvwy-ats.iot.us-east-2.amazonaws.com";
        private const int BrokerPort = 8883;
        private MqttClient IotClient;

        public Line Line;
        public WorkOrder WorkOrder;

        public void SentReportStatus()
        {
            if(!ConnectWithMqttBroker()) return;
            string message = JsonConvert.SerializeObject(this);
            string topic = $"Lines/Measurements";
            IotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
        }

        private bool ConnectWithMqttBroker()
        {
            try
            {
                string path = Directory.GetCurrentDirectory();
                var caCert = X509Certificate.CreateFromCertFile($"{path}/Certificates/root-CA.crt");
                var clientCert = new X509Certificate2($"{path}/Certificates/dotnet_devicecertificate.pfx", "uranio32");
                string clientId = Guid.NewGuid().ToString();
                IotClient = new MqttClient(IotEndpoint, BrokerPort, true, caCert, clientCert,
                    MqttSslProtocols.TLSv1_2);
                IotClient.Connect(clientId);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            
        }

    }
}
