﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using VoiceToFactoryMessages.Messages;

namespace machineSimulation
{
    public class Line
    {
        private bool _lineWorking;

        public int LineVelocity { get; set; }

        public string LineIdentifier { get; }

        public float LineTemperature { get; set; } = 90;

        public int PaperCoilPercentage { get; set; } = 100;

        public int SharpBladePercentage { get; set; } = 100; 

        public WorkOrder CurrentWorkOrder { get; set; }

        public Line(string lineIdentifier)
        {
            LineIdentifier = lineIdentifier;
        }

        public void GenerateReport() => Task.Run(() => ReportStatus());

        public void ConsumeWorkOrder() => Task.Run(() => DoWork());
        
        public bool LineWorking
        {
            get => _lineWorking;
            set
            {
                if (value)
                {
                    _lineWorking = true;
                    LineVelocity = 1;
                }
                else
                {
                    _lineWorking = false;
                    LineVelocity = 0;
                }
            }
        }

        public void SetVelocity(int velocidad) => LineVelocity = velocidad;

        public void SetMachineStatus(bool status) => _lineWorking = status;
        
        public void ReportStatus()
        {
            while (true)
            {
                ReportStatus status = new ReportStatus {Line = this, WorkOrder = CurrentWorkOrder};
                status.SentReportStatus();
                Console.WriteLine($"Report status for line {this.LineIdentifier}");
                Thread.Sleep(30000);
            }
        }

        public void ConsumeLineMessages()
        {
            try
            {
                string iotendpoint = "a2dqsjwbyvfvwy-ats.iot.us-east-2.amazonaws.com";
                int BrokerPort = 8883;
                string topic = $"Line{LineIdentifier}/machineOrder";
                var caCert = X509Certificate.CreateFromCertFile(@".\Certificates\root-CA.crt");
                var clientCert = new X509Certificate2(@".\Certificates\dotnet_devicecertificate.pfx", "uranio32");
                string clientId = Guid.NewGuid().ToString();
                var iotClient = new MqttClient(iotendpoint, BrokerPort, true, caCert, clientCert,
                    MqttSslProtocols.TLSv1_2);
                iotClient.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                iotClient.MqttMsgSubscribed += Client_MqttMsgSubscribed;
                iotClient.Connect(clientId);
                Console.WriteLine($"Connected to Line{LineIdentifier}/machineOrder");
                iotClient.Subscribe(new[] { topic }, new[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void ConsumeWorkOderMessages()
        {
            try
            {
                string iotendpoint = "a2dqsjwbyvfvwy-ats.iot.us-east-2.amazonaws.com";
                int BrokerPort = 8883;
                string topic = $"workorder/workorder";
                var caCert = X509Certificate.CreateFromCertFile(@".\Certificates\root-CA.crt");
                var clientCert = new X509Certificate2(@".\Certificates\dotnet_devicecertificate.pfx", "uranio32");
                string clientId = Guid.NewGuid().ToString();
                var iotClient = new MqttClient(iotendpoint, BrokerPort, true, caCert, clientCert,
                    MqttSslProtocols.TLSv1_2);
                iotClient.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                iotClient.MqttMsgSubscribed += Client_MqttMsgSubscribed;
                iotClient.Connect(clientId);
                Console.WriteLine("Connected to workorder/workorder");
                iotClient.Subscribe(new[] { topic }, new[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void Client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {
            Console.WriteLine("Message subscribed");
        }

        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                string message = System.Text.Encoding.UTF8.GetString(e.Message);
                Console.WriteLine("Message Received is" + message);
                if (message.Contains(nameof(LineChangeVelocityOrder)))
                {
                    var lineChangeVelocityOrder = JsonConvert.DeserializeObject<LineChangeVelocityOrder>(message);
                    if (lineChangeVelocityOrder != null)
                    {
                        switch (lineChangeVelocityOrder.Action)
                        {
                            case EVelocityAction.Decrease:
                                LineVelocity = LineVelocity - lineChangeVelocityOrder.Velocity;
                                if (LineVelocity < 0)
                                {
                                    LineVelocity = 0;
                                    LineWorking = false;
                                }
                                break;
                            case EVelocityAction.Increment:
                                LineVelocity = LineVelocity + lineChangeVelocityOrder.Velocity;
                                if (!LineWorking)
                                {
                                    LineWorking = true;
                                }
                                break;

                        }
                        
                    }
                }
                else if (message.Contains(nameof(LineStatusOrder)))
                {
                    var lineStatusOrder = JsonConvert.DeserializeObject<LineStatusOrder>(message);
                    if (lineStatusOrder != null)
                    {
                        LineWorking = lineStatusOrder.Status;
                    }

                    if (CurrentWorkOrder != null)
                    {
                        CurrentWorkOrder = null;
                    }
                }
                else if (message.Contains(nameof(WorkOrderAction)))
                {
                    var workOrderAction = JsonConvert.DeserializeObject<WorkOrderAction>(message);
                    if (workOrderAction != null)
                    {
                        switch (workOrderAction.Action) 
                        {
                            case EWorkOrderAction.Inicia:
                                CurrentWorkOrder = Program.WorkOrders.FirstOrDefault(x =>
                                    x.WorkOrderId.ToString().Equals(workOrderAction.WorkOrder));
                                if (CurrentWorkOrder != null)
                                    CurrentWorkOrder.Status = WorkOrderStatus.Iniciada;
                                break;
                            case EWorkOrderAction.Para:
                                if (Program.WorkOrders.FirstOrDefault(x =>
                                        x.WorkOrderId.ToString().Equals(workOrderAction.WorkOrder)) != null)
                                {
                                    Program.WorkOrders.First(x =>
                                            x.WorkOrderId.ToString().Equals(workOrderAction.WorkOrder)).Status =
                                        WorkOrderStatus.Parada;
                                }
                                CurrentWorkOrder = null;
                                break;
                            case EWorkOrderAction.Reanuda:
                                if (CurrentWorkOrder != null)
                                    CurrentWorkOrder.Status = WorkOrderStatus.Iniciada;
                                break;
                            case EWorkOrderAction.Pausa:
                                if (CurrentWorkOrder != null)
                                    CurrentWorkOrder.Status = WorkOrderStatus.Pausada;
                                break;
                        }
                    }
                    
                }
                else if (message.Contains(nameof(AddUnit)))
                {
                    var addunitMessage = JsonConvert.DeserializeObject<AddUnit>(message);
                    if(addunitMessage?.UnitsToadd > 0 && CurrentWorkOrder?.WorkOrderId.ToString() == addunitMessage.WorkOrder)
                    {
                        if (CurrentWorkOrder != null)
                            CurrentWorkOrder.TotalUnits = CurrentWorkOrder.TotalUnits + addunitMessage.UnitsToadd;
                    }

                }
                else if (message.Contains(nameof(VoiceToFactoryMessages.Messages.AddWorkOrderToLine)))
                {
                    var addWorkOrderToLineMessage = JsonConvert.DeserializeObject<AddWorkOrderToLine>(message);
                    if (CurrentWorkOrder.WorkOrderId.ToString() == addWorkOrderToLineMessage.WorkOrder &&
                        int.TryParse(addWorkOrderToLineMessage.WorkOrder, out var order))
                        AddWorkOrderToLine(order);
                }
                else if (message.Contains(nameof(AddMeasurement)))
                {
                    var addMeasurementMessage = JsonConvert.DeserializeObject<AddMeasurement>(message);
                    if (CurrentWorkOrder.WorkOrderId.ToString() == addMeasurementMessage.WorkOrder)
                        AddAnnotationToWorkOrder(addMeasurementMessage.MeasurementType);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            
        }

        private void AddWorkOrderToLine(int workOrder)
        {
            WorkOrder orderSelected = Program.WorkOrders.FirstOrDefault(x => x.WorkOrderId.Equals(workOrder));
            if(orderSelected != null)
            {
                CurrentWorkOrder = orderSelected;
            }
        }

        private void AddAnnotationToWorkOrder(string key)
        {
            switch (key.ToLower())
            {
                case "corte":
                    CurrentWorkOrder?.AddAnotation(key, SharpBladePercentage.ToString());
                    break;
                case "velocidad":
                    CurrentWorkOrder?.AddAnotation(key, LineVelocity.ToString());
                    break;
                case "temperatura":
                    CurrentWorkOrder?.AddAnotation(key, LineTemperature.ToString());
                    break;
            }        
        }

        private void DoWork()
        {
            while (true)
            {
                if (LineWorking && CurrentWorkOrder != null && CurrentWorkOrder.Status.Equals(WorkOrderStatus.Iniciada))
                {
                    PaperCoilPercentage--;
                    SharpBladePercentage--;
                    if(CurrentWorkOrder.CurrentProducedUnit < CurrentWorkOrder.TotalUnits)
                        CurrentWorkOrder.CurrentProducedUnit += LineVelocity;
                }
                if(LineVelocity == 0)
                    Thread.Sleep(60000);
                else
                    Thread.Sleep(60000 / LineVelocity);
            }
            
        }
    }
}
