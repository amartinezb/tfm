﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace machineSimulation
{
    class Program
    {
        public static List<WorkOrder> WorkOrders;
        static void Main(string[] args)
        {
            Console.WriteLine("Generando Ordenes de trabajo");
            GenerateWorkOrders();
            Console.WriteLine("Arrancando Simulador de Maquina de Linea");
            Line line1 = new Line("a");
            line1.GenerateReport();
            line1.ConsumeWorkOrder();
            line1.ConsumeLineMessages();
            line1.ConsumeWorkOderMessages();
            //Line line2 = new Line("B");
            //Line line3 = new Line("C");
            //Line line4 = new Line("D");
            Console.ReadLine();
        }


        public static void GenerateWorkOrders()
        {
            WorkOrders = new List<WorkOrder>();
            WorkOrder workOrder1 = new WorkOrder(1,100);
            WorkOrder workOrder2 = new WorkOrder(2, 50);
            WorkOrder workOrder3 = new WorkOrder(3, 20);
            WorkOrder workOrder4 = new WorkOrder(4, 80);
            WorkOrders.Add(workOrder1);
            WorkOrders.Add(workOrder2);
            WorkOrders.Add(workOrder3);
            WorkOrders.Add(workOrder4);
        }   
    }
}
