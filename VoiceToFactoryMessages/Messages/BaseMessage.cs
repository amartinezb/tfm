﻿namespace VoiceToFactoryMessages.Messages
{
    interface IBaseMessage
    {
        string FunctionName { get; }
    }
}
