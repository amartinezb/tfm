﻿namespace VoiceToFactoryMessages.Messages
{
    public class AddWorkOrderToLine : IBaseMessage
    {
        public string FunctionName => nameof(AddWorkOrderToLine);

        public string Line;

        public string WorkOrder { get; }

        public AddWorkOrderToLine(string line, string workOrder)
        {
            Line = line;
            WorkOrder = workOrder;
        }
    }
}
