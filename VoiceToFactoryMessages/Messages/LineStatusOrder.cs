﻿namespace VoiceToFactoryMessages.Messages
{
    public class LineStatusOrder : IBaseMessage
    {
        public string LineIdentification { get; set; }
        public bool Status { get; set; }

        public string FunctionName => nameof(LineStatusOrder);

        public LineStatusOrder(string lineIdentification, bool status)
        {
            LineIdentification = lineIdentification;
            this.Status = status;
        }
    }
}
