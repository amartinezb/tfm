﻿namespace VoiceToFactoryMessages.Messages
{
    /// <summary>
    /// Add unit class
    /// </summary>
    /// <seealso cref="IBaseMessage" />
    public class AddUnit : IBaseMessage
    {
        /// <summary>
        /// Gets the name of the function.
        /// </summary>
        /// <value>
        /// The name of the function.
        /// </value>
        public string FunctionName => nameof(AddUnit);
        /// <summary>
        /// Gets or sets the units toadd.
        /// </summary>
        /// <value>
        /// The units toadd.
        /// </value>
        public int UnitsToadd { get; set; }
        /// <summary>
        /// Gets or sets the work order.
        /// </summary>
        /// <value>
        /// The work order.
        /// </value>
        public string WorkOrder { get; set; }

        public AddUnit(string workOrder, int unitsToadd)
        {
            WorkOrder = workOrder;
            UnitsToadd = unitsToadd;
        }
    }
}
