﻿namespace VoiceToFactoryMessages.Messages
{
    public class WorkOrderAction : IBaseMessage
    {
        public string FunctionName => nameof(WorkOrderAction);

        public EWorkOrderAction Action;

        public string WorkOrder { get; }

        public string Line { get; }

        public WorkOrderAction(string action, string workOrder, string line)
        {
            Line = line;
            WorkOrder = workOrder;
            switch (action.ToLower())
            {
                case "arranca":
                case "1":
                    Action = EWorkOrderAction.Inicia;
                    break;
                case "termina":
                case "2":
                    Action = EWorkOrderAction.Para;
                    break;
                case "pausa":
                case "3":
                    Action = EWorkOrderAction.Pausa;
                    break;
                case "reanuda":
                case "4":
                    Action = EWorkOrderAction.Reanuda;
                    break;
            }
        }
    }

    public enum EWorkOrderAction
    {
        Inicia = 1,
        Para = 2,
        Pausa = 3,
        Reanuda = 4
    }
}
