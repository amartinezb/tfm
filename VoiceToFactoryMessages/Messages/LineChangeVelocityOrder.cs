﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace VoiceToFactoryMessages.Messages
{
    public class LineChangeVelocityOrder : IBaseMessage
    {
        public LineChangeVelocityOrder(string lineIdentification, string action ,int velocity)
        {
            LineIdentification = lineIdentification;
            Velocity = velocity;
            switch (action.ToLower())
            {
                case "increment":
                case "aumentavelocidad":
                case "1":
                    Action = EVelocityAction.Increment;
                    break;
                case "disminuyevelocidad":
                case "decrease":
                case "2":
                    Action = EVelocityAction.Decrease;
                    break;
            }
        }

        public string LineIdentification { get; }
        public int Velocity { get; }
        public string FunctionName => nameof(LineChangeVelocityOrder);
        [JsonConverter(typeof(StringEnumConverter))]
        public EVelocityAction Action;

    }

    public enum EVelocityAction
    {
        Increment = 1,
        Decrease = 2
    }
}
