﻿namespace VoiceToFactoryMessages.Messages
{
    public class AddMeasurement : IBaseMessage
    {
        public string FunctionName => nameof(AddMeasurement);

        public string WorkOrder;

        public string MeasurementType;

        public string MeasurementValue;

        public AddMeasurement(string workOrder, string measurementType, string measurementValue)
        {
            WorkOrder = workOrder;
            MeasurementType = measurementType;
            MeasurementValue = measurementValue;
        }

    }
}
