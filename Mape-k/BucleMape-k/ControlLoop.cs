﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using VoiceToFactoryMessages.Messages;

namespace BucleMape_k
{
    class ControlLoop
    {
        private bool _preventionPlanTemperature;
        private bool _executionPlanTemperature;
        private bool _preventionPlanCoil;
        private bool _executionPlanCoil;
        private bool _preventionPlanBlade;
        private bool _executionPlanBlade;
        private MqttClient iotClient;

        public ControlLoop()
        {
            Console.WriteLine("Running Control loop");
            Console.WriteLine("Subscribing to ReportStatus topic");
            Subscribe();
            Console.ReadLine();
        }

        public void Subscribe()
        {
            try
            {
                string iotendpoint = "a2dqsjwbyvfvwy-ats.iot.us-east-2.amazonaws.com";
                int BrokerPort = 8883;
                string topic = "Lines/Measurements";
                var caCert = X509Certificate.CreateFromCertFile($"{AppDomain.CurrentDomain.BaseDirectory}\\Certificates\\root-CA.crt");
                var clientCert = new X509Certificate2($"{AppDomain.CurrentDomain.BaseDirectory}\\Certificates\\dotnet_devicecertificate.pfx", "uranio32");
                string clientId = Guid.NewGuid().ToString();
                iotClient = new MqttClient(iotendpoint, BrokerPort, true, caCert, clientCert,
                    MqttSslProtocols.TLSv1_2);
                iotClient.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
                iotClient.MqttMsgSubscribed += Client_MqttMsgSubscribed;
                iotClient.Connect(clientId);
                Console.WriteLine($"Connected to topic {topic}");
                iotClient.Subscribe(new[] { topic }, new[] { MqttMsgBase.QOS_LEVEL_AT_LEAST_ONCE });
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void Client_MqttMsgSubscribed(object sender, MqttMsgSubscribedEventArgs e)
        {
            Console.WriteLine("Message subscribed");
        }

        private void Client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            try
            {
                string message = System.Text.Encoding.UTF8.GetString(e.Message);
                Console.WriteLine($"******************************************");
                Console.WriteLine("Message Received is" + message);
                ReportStatus reportStatus = JsonConvert.DeserializeObject<ReportStatus>(message);
                int coilStatus;
                float temperature;
                int blade;
                MonitorPhase(reportStatus, out coilStatus, out temperature, out blade);
                Console.WriteLine($"------------------------------------------");
                AnalysisPhase(coilStatus, temperature, blade);
                Console.WriteLine($"------------------------------------------");
                PlanningPhase(coilStatus,temperature,blade);
                Console.WriteLine($"------------------------------------------");
                ExecutionPhase(reportStatus);
                Console.WriteLine($"******************************************");
            }
            catch (Exception ex)
            {

            }
        }

        private void ExecutionPhase(ReportStatus reportStatus)
        {
            ExecuteTemperature(reportStatus);
            ExecuteCoil(reportStatus);
            ExecuteBlade(reportStatus);
        }

        private void ExecuteBlade(ReportStatus reportStatus)
        {
            if (_preventionPlanBlade)
            {
                ReduceVelocity(reportStatus);
            }
            else if (_executionPlanBlade)
            {
                StopMachine(reportStatus);
            }
        }

        private void ExecuteCoil(ReportStatus reportStatus)
        {
            if (_preventionPlanCoil)
            {
                ReduceVelocity(reportStatus);
            }
            else if (_executionPlanCoil)
            {
                StopMachine(reportStatus);
            }
        }

        private void ExecuteTemperature(ReportStatus reportStatus)
        {
            if (_preventionPlanTemperature)
            {
                ReduceVelocity(reportStatus);
            }
            else if (_executionPlanTemperature)
            {
                StopMachine(reportStatus);
            }
        }

        private void PlanningPhase(int coilStatus, float temperature, int blade)
        {
            Console.WriteLine($"Planning phase");
            PlanningTemperature(temperature);
            PlanningCoil(coilStatus);
            PlanningBlade(blade);
        }

        private void PlanningBlade(int blade)
        {
            if (blade > 10 && blade < 20)
            {
                Console.WriteLine($"Preventive action for Blade status planned");
                _preventionPlanBlade = true;
            }
            else if (blade < 10)
            {
                Console.WriteLine($"Corrective action for Blade status planned");
                _executionPlanBlade = true;
            }
            else
            {
                Console.WriteLine($"No action for blade status planned");
            }
        }

        private void PlanningCoil(int coilStatus)
        {
            if (coilStatus > 10 && coilStatus < 20)
            {
                Console.WriteLine($"Preventive action for Coil status planned");
                _preventionPlanCoil = true;
            }
            else if (coilStatus < 10)
            {
                Console.WriteLine($"Corrective action for Coil status planned");
                _executionPlanCoil = true;
            }
            else
            {
                Console.WriteLine($"No action for Coil status planned");
            }
        }

        private void PlanningTemperature(float temperature)
        {
            if (temperature > 100 && temperature < 120)
            {
                Console.WriteLine($"Preventive action for temperature planned");
                _preventionPlanTemperature = true;
            }
            else if (temperature >= 120)
            {
                Console.WriteLine($"Corrective action for temperature planned");
                _executionPlanTemperature = true;
            }
            else
            {
                Console.WriteLine($"No action for temperature planned");
            }
        }

        private void StopMachine(ReportStatus reportStatus)
        {
            try
            {
                string topic = $"Line{reportStatus.Line.LineIdentifier}/machineOrder";
                LineStatusOrder order = new LineStatusOrder(reportStatus.Line.LineIdentifier, false);
                var message = JsonConvert.SerializeObject(order);
                iotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                Console.WriteLine("Deteniendo maquina como accion correctiva");
            }
            catch (Exception)
            {
                 //Ignored
            }
        }

        private void ReduceVelocity(ReportStatus reportStatus)
        {
            try
            {
                string topic = $"Line{reportStatus.Line.LineIdentifier}/machineOrder";
                LineChangeVelocityOrder order = new LineChangeVelocityOrder(reportStatus.Line.LineIdentifier, "DisminuyeVelocidad", 5);
                var message = JsonConvert.SerializeObject(order);
                iotClient?.Publish(topic, Encoding.UTF8.GetBytes(message));
                Console.WriteLine("Disminuyendo la velocidad de la maquina como accion preventiva");
            }
            catch (Exception)
            {
                //ignored
            }
        }

        private static void AnalysisPhase(int coilStatus, float temperature, int blade)
        {
            Console.WriteLine($"Analize phase");
            Console.WriteLine($"Analyzing temperature");
            if (temperature > 100 && temperature < 120)
            {
                Console.WriteLine($"Overheat detected");
            }
            else if (temperature >= 120)
            {
                Console.WriteLine($"Dangerous temperature detected");
            }
            else
            {
                Console.WriteLine($"Normal temperature detected");
            }
            Console.WriteLine($"Analyzing Coil status");
            if (coilStatus > 0 && coilStatus < 20)
            {
                Console.WriteLine($"Coil is close to be empty");
            }
            else if (coilStatus == 0)
            {
                Console.WriteLine($"Coil is empty");
            }
            else
            {
                Console.WriteLine($"Coil quantity is  correct");
            }
            Console.WriteLine($"Analyzing blade sharp status");
            if (blade > 0 && blade < 20)
            {
                Console.WriteLine($"blade is close to be blunt");
            }
            else if (coilStatus == 0)
            {
                Console.WriteLine($"blade is blunt");
            }
            else
            {
                Console.WriteLine($"Blade sharp is correct");
            }
        }

        private static void MonitorPhase(ReportStatus reportStatus, out int coilStatus, out float temperature, out int blade)
        {
            Console.WriteLine($"Monitor phase, data received for line {reportStatus.Line.LineIdentifier}  is");
            Console.WriteLine($"Coil status percentage is {reportStatus.Line.PaperCoilPercentage}");
            coilStatus = reportStatus.Line.PaperCoilPercentage;
            Console.WriteLine($"Blade status percentage is {reportStatus.Line.SharpBladePercentage}");
            blade = reportStatus.Line.SharpBladePercentage;
            Console.WriteLine($"Temperature is {reportStatus.Line.LineTemperature}");
            temperature = reportStatus.Line.LineTemperature;
        }
    }
}
