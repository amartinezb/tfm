﻿using System.Collections.Generic;

namespace BucleMape_k
{
    public class ReportStatus
    {
        public Line Line;
        public WorkOrder WorkOrder;
    }

    public class Line
    {
        private bool _lineWorking;

        public int LineVelocity { get; set; }

        public string LineIdentifier { get; }

        public float LineTemperature { get; set; } = 90;

        public int PaperCoilPercentage { get; set; } = 100;

        public int SharpBladePercentage { get; set; } = 100;

        public WorkOrder CurrentWorkOrder { get; set; }

        public Line(string lineIdentifier)
        {
            LineIdentifier = lineIdentifier;
        }
    }

    public class WorkOrder
    {
        public int WorkOrderId;

        public WorkOrderStatus Status;

        public Dictionary<string, string> Anotations;

        public int CurrentProducedUnit;

        public int TotalUnits;
    }

    public enum WorkOrderStatus
    {
        Iniciada,
        Pausada,
        Parada
    }


}
